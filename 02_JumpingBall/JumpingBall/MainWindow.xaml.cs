﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace JumpingBall
{
    
    public partial class MainWindow : Window
    {
        DispatcherTimer timer = new DispatcherTimer();
        public MainWindow()
        {
            InitializeComponent();
            timer.Tick += Physics;

            timer.Interval = TimeSpan.FromSeconds(0.05);
        }

        

        
       

        private void radioButtonBlue_Click(object sender, RoutedEventArgs e)
        {
            Ball.Fill = Brushes.Blue;
        }

        private void radioButtonRed_Click(object sender, RoutedEventArgs e)
        {
            Ball.Fill = Brushes.Red;
        }

        private void radioButtonGreen_Click(object sender, RoutedEventArgs e)
        {
            Ball.Fill = Brushes.Green;
        }

        private void ButtonStartStop_Click(object sender, RoutedEventArgs e)
        {
            timer.IsEnabled = !timer.IsEnabled;
        }
        bool GoingRight = true;
        bool GoingDown = true;

        private void Physics(object sender,EventArgs e)
        {
            double speed = 3.0;

            if(CheckBoxFast.IsChecked.Value)
            {
                speed = 10.0;
            }
            MoveBallLeftRight(speed);
            MoveBallUpDown(speed);
        }
        private void MoveBallLeftRight(double speed)
        {
            double x = Canvas.GetLeft(Ball);
            if (GoingRight)
                x += speed;
            else
                x -= speed;

            if(x + Ball.Width > TheCanvas.ActualWidth)
            {
                GoingRight = false;
                x = TheCanvas.ActualWidth - Ball.Width;
                
            }
            else if(x < 0.0)
            {
                GoingRight = true;
                x = 0.0;
            }
            Canvas.SetLeft(Ball,x);
        }
        private void MoveBallUpDown(double speed)
        {
            double y = Canvas.GetTop(Ball);
            if (GoingDown)
                y += speed;
            else
                y -= speed;

            if (y + Ball.Height > TheCanvas.ActualHeight)
            {
                GoingDown = false;
                y = TheCanvas.ActualHeight - Ball.Height;

            }
            else if (y < 0.0)
            {
                GoingDown = true;
                y = 0.0;
            }
            Canvas.SetTop(Ball, y);
        }

        int score = 0;

        private void Ball_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if(timer.IsEnabled)
            {
                score++;
                labelScore.Content = score;
            }
        }
    }
}
