﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facade_Sek
{
    class Program
    {
        static void Main(string[] args)
        {
            Sekretaer s = new Sekretaer();
            s.dokumentVersenden("hallo alla");
        }
    }

    class Sekretaer
    {
        public void dokumentVersenden(string text)
        {
            Computer computer = new Computer();
            Drucker drucker = new Drucker();
            Stift stift = new Stift();
            Stempel stempel = new Stempel();
            Briefmarkenautomat briefmarkenautomat = new Briefmarkenautomat();
            Briefkasten briefkasten = new Briefkasten();
            
            computer.an();
           
            Textverarbeitungsprogram textverarbeitungsprog = computer.GetTextverarbeitungsprogram();
            textverarbeitungsprog.oeffen();
           
            Dokument dokument = textverarbeitungsprog.GetDokument(text);
            
            drucker.an();
           
            drucker.konfigurieren();
            
            drucker.papierNachfuellen();
            
            computer.druckeDokument(dokument);
           
            drucker.aus();
            
            computer.aus();
       
            stift.unterschreiben(dokument);
          
            stempel.stempeln(dokument);
           
            briefmarkenautomat.briefmarkeBezahlt(dokument, 2);
           
            briefkasten.briefEinwerfen(dokument);
        }
    }
    class Dokument
    {
        private bool stembel;
        public bool Stembel { get; set; }
        private bool unterschrieben;
        public bool Unterschieben { get; set; }
        private string text;
        public string Text { get; set; }
    }
    class Computer
    {
        public void an()
        {
            Console.WriteLine("Der Computer ist jetzt an");
        }
        public void aus()
        {
            Console.WriteLine("Der Computer ist jetzt aus");
        }
        public void druckeDokument(Dokument dokument)
        {
            Console.WriteLine(dokument.Text + " wurde gedruckt");
        }
        public Textverarbeitungsprogram GetTextverarbeitungsprogram()
        {
            return new Textverarbeitungsprogram();
        }
    }

    class Textverarbeitungsprogram
    {
        Dokument t;

        public Textverarbeitungsprogram()
        {
             t = new Dokument();
        }
        
        public Dokument GetDokument(string text)
        {
            
            t.Text += text;
            return t;
            
        }
        public void oeffen()
        {
            Console.WriteLine("Dokument geöffnet");
        }
        public void schliessen()
        {
            Console.WriteLine("Dokument schließt");
        }

    }
    class Stift
    {
        public void unterschreiben(Dokument d)
        {
            d.Unterschieben = true;
            Console.WriteLine("Dokument wurde unterschrieben");
        }
    }
    class Stempel
    {
        public void stempeln(Dokument d)
        {
            d.Stembel = true;
            Console.WriteLine("Dokument wurde gestempelt");

        }
    }
    class Briefmarkenautomat
    {
        public void briefmarkeBezahlt(Dokument d, int t)
        {
            Console.WriteLine("Das Dokument mit dem text " + d.Text + " wurde frakiert");
        }
    }
    class Briefkasten
    {
        public void briefEinwerfen(Dokument d)
        {
            Console.WriteLine("Das Dokument mit dem text " + d.Text + " wurde eingeworfen");
        }
    }
    class Drucker
    {
        public void an()
        {
            Console.WriteLine("Drucker ist jetzt an");
        }
        public void aus()
        {
            Console.WriteLine("Drucker ist jetzt aus");
        }
        public void konfigurieren()
        {
            Console.WriteLine("Drucker wird konfiguriert");
        }
        public void papierNachfuellen()
        {
            Console.WriteLine("Papier wird nachgefüllt");
        }
    }
}
