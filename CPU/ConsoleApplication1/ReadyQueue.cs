﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Concurrent;

namespace ConsoleApplication1
{
    class ReadyQueue : BlockingCollection<MyThread>
    {
        private static ReadyQueue ourInstance;

        public ReadyQueue()
        {

        }

        public static ReadyQueue getInstance()
        {
            if (ourInstance == null)
                ourInstance = new ReadyQueue();
            return ourInstance;
        }
    }
}
