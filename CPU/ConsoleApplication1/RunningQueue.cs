﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks;
using System.Collections.Concurrent;

namespace ConsoleApplication1
{
    class RunningQueue : BlockingCollection<MyThread>
    {
        private static RunningQueue ourInstance;

        public RunningQueue()
        {

        }

        public static RunningQueue getInstance()
        {
            if (ourInstance == null)
                ourInstance = new RunningQueue();
            return ourInstance;
        }
    }
}
