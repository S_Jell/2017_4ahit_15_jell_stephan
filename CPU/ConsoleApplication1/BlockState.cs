﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class BlockState : IState
    {
        public IState changeState(MyThread mt)
        {
            Console.WriteLine(toString() + " " + mt.getName());
            return new ReadyState();
        }

        public string toString()
        {
            return "Block state:";
        }
    }
}
