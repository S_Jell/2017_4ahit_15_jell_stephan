﻿using System;
using Adapter_Offshore;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AccountTest
{
    [TestClass]
    public class AccountTest
    {
        [TestMethod]
        public void check_accountBalance()
        {
            double balance = 2000;

            StandardAccount c = new StandardAccount(2000);

            Assert.AreEqual(balance,c.getBalance());
        }
        [TestMethod]
        public void check_offshore_balance()
        {
            double balance = 1920;
            AccountAdapter ac = new AccountAdapter(new OffshoreAccount(2000));
            Assert.AreEqual(balance,ac.getBalance());
        }

        [TestMethod]
        public void check_standardaccount_overdraft()
        {
            StandardAccount s = new StandardAccount(1);
            Assert.IsFalse(s.isOverdraftAvailable());
        }

        [TestMethod]
        public void check_platinumaccount_overdraft()
        {
            PlatinumAccount s = new PlatinumAccount(1);
            Assert.IsTrue(s.isOverdraftAvailable());
        }
    }
}
