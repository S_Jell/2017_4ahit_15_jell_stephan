﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adapter_Offshore
{
    class Program
    {
        static void Main(string[] args)
        {
            StandardAccount sa = new StandardAccount(2000);
            Console.WriteLine(sa.getBalance());
           

            AccountAdapter adapter = new AccountAdapter(new OffshoreAccount(2000));
            Console.WriteLine(adapter.getBalance());

            Console.ReadLine();
        }
    }
}
