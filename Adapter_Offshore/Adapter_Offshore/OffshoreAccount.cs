﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adapter_Offshore
{
    public class OffshoreAccount
    {
        double balance;

        double TAX_RATE = 0.04;

        public OffshoreAccount(double balance)
        {
            this.balance = balance;
        }
        public double get_TAX_RATE()
        {
            return TAX_RATE;
        }
        public double getOffshoreBalance()
        {
            return balance;
        }
        public void debit(double debit)
        {
            if(balance >= debit)
            {
                balance -= debit;
            }
        }
        public void credit(double credit)
        {
            balance += credit;
        }
    }
}
