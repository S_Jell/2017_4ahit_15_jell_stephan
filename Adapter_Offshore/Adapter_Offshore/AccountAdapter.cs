﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adapter_Offshore
{
    public class AccountAdapter:AbstractAccount
    {
        private OffshoreAccount offShoreAccount;

        public AccountAdapter(OffshoreAccount offshoreAccount):base(offshoreAccount.getOffshoreBalance())
        {
            this.offShoreAccount = offshoreAccount;
        }
        public double getBalance()
        {
            double taxRate = offShoreAccount.get_TAX_RATE();
            double grossBalance = offShoreAccount.getOffshoreBalance();

            double taxableBalance = grossBalance * taxRate;
            double balanceAfterTax = grossBalance - taxableBalance;
            return balanceAfterTax;
        }
       
    }
}
