﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adapter_Offshore
{
    public class AbstractAccount : Account
    {
        double balance;
        bool isOverdraft;

        public AbstractAccount(double balance)
        {
            this.balance = balance;
        }
        public void credit(double credit)
        {
            balance += credit;
        }

        public double getBalance()
        {
            return balance;
        }
        public void setOverdraftAvailable(Boolean tmp)
        {
            this.isOverdraft = tmp;
        }

        public bool isOverdraftAvailable()
        {
            return isOverdraft;
        }
        public string ToString()
        {
            return "Balance: " + balance + " " + "Overdraft: " + isOverdraft;
        }
    }
}
