﻿using SEW_StudentBook_Applikation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BookOrderStudentApplication
{
    /// <summary>
    /// Interaktionslogik für StudentView.xaml
    /// </summary>
    public partial class StudentView : Window
    {
        public StudentRepository student_repo=new StudentRepository();
        string firstname;
        string lastname;
        int age;
        public StudentView()
        {
            InitializeComponent();
        }
        private void listbox_selectItem(object sender, SelectionChangedEventArgs e)
        {
           
        }
        

        private void btn_update_Click(object sender, RoutedEventArgs e)
        {
            RelayCommand r = new RelayCommand(UpdateInDB);
            if (r.CanExecute(new Object()))
            {
                r.Execute(new Object());
            }
        }

        private void btn_delete_Click(object sender, RoutedEventArgs e)
        {
            RelayCommand r = new RelayCommand(DeleteInDB);
            if (r.CanExecute(new Object()))
            {
                r.Execute(new Object());
            }
        }

       

        private void btn_create_Click(object sender, RoutedEventArgs e)
        {
            RelayCommand r = new RelayCommand(InsertInDB);
            if (r.CanExecute(new Object()))
            {
                r.Execute(new Object());
            }
        }
       

        private void listbox_selectedItem(object sender, MouseButtonEventArgs e)
        {
            DataGrid d = sender as DataGrid;
            Student row = (Student)listbox_entities.SelectedItem;

            int i = row.ID;
            Student s = student_repo.GetEntityByID(i);
            txt_firstname.Text = s.FirstName;
            txt_age.Text = Convert.ToString(s.Age);
            txt_lastname.Text = s.LastName;

            lastname = txt_lastname.Text;
            age = Convert.ToInt32(txt_age.Text);
            firstname = txt_firstname.Text;
        }

        public void UpdateInDB()
        {
            lastname = txt_lastname.Text;
            age = Convert.ToInt32(txt_age.Text);
            firstname = txt_firstname.Text;
            int i = Convert.ToInt32(listbox_entities.SelectedValue.ToString());
            Student s = student_repo.GetEntityByID(i);
            s.Age = age;
            s.FirstName = firstname;
            s.LastName = lastname;


            student_repo.Update(s);
            StudentVM t = (StudentVM)this.DataContext;
            ObservableCollection<Student> k = t.Students;
            foreach (Student s2 in k)
            {
                if (s.ID == s2.ID)
                {
                    s2.LastName = s.LastName;
                    s2.FirstName = s.FirstName;
                    s2.Age = s.Age;
                }
            }
            t.RaiseChange("Students");
        }
        public void InsertInDB()
        {
            lastname = txt_lastname.Text;
            age = Convert.ToInt32(txt_age.Text);
            firstname = txt_firstname.Text;

            Student s = new Student();
            s.Age = age;
            s.FirstName = firstname;
            s.LastName = lastname;

            student_repo.Insert(s);
            StudentVM t = (StudentVM)this.DataContext;
            t.Refresh();
        }
        public void DeleteInDB()
        {
            Student s = student_repo.GetEntityByID(Convert.ToInt32(listbox_entities.SelectedValue.ToString()));
            student_repo.Delete(s.ID);
            StudentVM t = (StudentVM)this.DataContext;
            t.Refresh();
        }
    }
}
