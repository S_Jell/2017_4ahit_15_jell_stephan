﻿
using BookOrderStudentApplication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SEW_StudentBook_Applikation
{
    /// <summary>
    /// Interaktionslogik für FilterResults.xaml
    /// </summary>
    public partial class FilterResults : Window
    {
        List<Book> resultsbooks;
        List<Order> resultsorders;
        List<Student> resultsstudents;
        public FilterResults()
        {
            InitializeComponent();
        }

        public void BookFilter(List<Book> results)
        {
            resultsbooks = results;
            datag_results.ItemsSource = resultsbooks;
        }
        public void OrderFilter(List<Order> results)
        {
            resultsorders = results;
            datag_results.ItemsSource = resultsorders;
        }
        public void StudentFilter(List<Student> results)
        {
            resultsstudents = results;
            datag_results.ItemsSource = resultsstudents;
        }
    }
}
