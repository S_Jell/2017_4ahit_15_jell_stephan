﻿using SEW_StudentBook_Applikation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BookOrderStudentApplication
{
    /// <summary>
    /// Interaktionslogik für OrderView.xaml
    /// </summary>
    public partial class OrderView : Window
    {
        public OrderRepository or = new OrderRepository();
        int b_id;
        int s_id;
        DateTime orderDate;
        public OrderView()
        {
            InitializeComponent();
        }

        private void listbox_selectItem(object sender, MouseButtonEventArgs e)
        {
            Order o = or.GetEntityByID(Convert.ToInt32(listbox_entities.SelectedValue.ToString()));
            txt_bid.Text = Convert.ToString(o.Book_ID);
            txt_sid.Text = Convert.ToString(o.Student_ID);
            txt_orderdate.Text = Convert.ToString(o.orderDate);
            b_id = o.Book_ID;
            s_id = o.Student_ID;
            orderDate = o.orderDate;
        }

        private void btn_create_Click(object sender, RoutedEventArgs e)
        {
            RelayCommand r = new RelayCommand(InsertInDB);
            if (r.CanExecute(new Object()))
            {
                r.Execute(new Object());
            }
        }

        private void btn_update_Click(object sender, RoutedEventArgs e)
        {
            RelayCommand r = new RelayCommand(UpdateInDB);
            if (r.CanExecute(new Object()))
            {
                r.Execute(new Object());
            }
        }

        private void btn_delete_Click(object sender, RoutedEventArgs e)
        {
            RelayCommand r = new RelayCommand(DeleteInDB);
            if (r.CanExecute(new Object()))
            {
                r.Execute(new Object());
            }
        }

        public void InsertInDB()
        {
            b_id = Convert.ToInt32(txt_bid.Text);
            s_id = Convert.ToInt32(txt_sid.Text);
            orderDate = Convert.ToDateTime(txt_orderdate.Text);
            Order o = new Order();
            o.Book_ID = b_id;
            o.Student_ID = s_id;
            o.orderDate = orderDate;

            or.Insert(o);
            StudentBookVM t = (StudentBookVM)this.DataContext;
            t.Refresh();
        }

        public void UpdateInDB()
        {
            b_id = Convert.ToInt32(txt_bid.Text);
            s_id = Convert.ToInt32(txt_sid.Text);
            orderDate = Convert.ToDateTime(txt_orderdate.Text);
            Order o = or.GetEntityByID(Convert.ToInt32(listbox_entities.SelectedValue.ToString()));
            o.Book_ID = b_id;
            o.Student_ID = s_id;
            o.orderDate = orderDate;
            or.Update(o);
            StudentBookVM t = (StudentBookVM)this.DataContext;
            t.Refresh();
            ObservableCollection<Order> k = t.StudentBooks;
            foreach (Order o2 in k)
            {
                if (o.ID == o2.ID)
                {
                    o2.Book_ID = o.Book_ID;
                    o2.Student_ID = o.Student_ID;
                    o2.orderDate = o.orderDate;
                }
            }
            t.RaiseChange("Students");
        }

        public void DeleteInDB()
        {
            Order o = or.GetEntityByID(Convert.ToInt32(listbox_entities.SelectedValue.ToString()));
            or.Delete(o.ID);
            StudentBookVM t = (StudentBookVM)this.DataContext;
            t.Refresh();
        }

       
    }
}
