﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SEW_StudentBook_Applikation
{
    public class RelayCommand:ICommand
    {
        private Action commandTask;

        public RelayCommand(Action worktodo)
        {
            commandTask = worktodo;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            commandTask();
        }
    }
}
