﻿using SEW_StudentBook_Applikation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BookOrderStudentApplication
{
    /// <summary>
    /// Interaktionslogik für BookView.xaml
    /// </summary>
    public partial class BookView : Window
    {
        public BookRepository book_repo;
        string title;
        decimal price;
        string author;

        public BookView()
        {
            InitializeComponent();
            book_repo = new BookRepository();
        }

        private void listbox_selectItem(object sender, MouseButtonEventArgs e)
        {
            Book b = book_repo.GetEntityByID(Convert.ToInt32(listbox_entities.SelectedValue.ToString()));
            txt_title.Text = b.Title;
            txt_price.Text = Convert.ToString(b.Price);
            txt_author.Text = b.Author;

            title = txt_title.Text;
            price = Convert.ToDecimal(txt_price.Text);
            author = txt_author.Text;
        }
       

        private void btn_create_Click(object sender, RoutedEventArgs e)
        {
            RelayCommand r = new RelayCommand(InsertInDB);
            if (r.CanExecute(new Object())) {
                r.Execute(new Object());
            }
        }

        private void btn_update_Click(object sender, RoutedEventArgs e)
        {
            RelayCommand r = new RelayCommand(UpdateInDB);
            if (r.CanExecute(new Object()))
            {
                r.Execute(new Object());
            }
        }

        private void btn_delete_Click(object sender, RoutedEventArgs e)
        {
            RelayCommand r = new RelayCommand(DeleteInDB);
            if (r.CanExecute(new Object()))
            {
                r.Execute(new Object());
            }
        }

        public void DeleteInDB()
        {
            Book b = book_repo.GetEntityByID(Convert.ToInt32(listbox_entities.SelectedValue.ToString()));
            book_repo.Delete(b.ID);
            BookVM t = (BookVM)this.DataContext;
            t.Refresh();
        }

        public void UpdateInDB()
        {
            title = txt_title.Text;
            price = Convert.ToDecimal(txt_price.Text);
            author = txt_author.Text;

            Book b = book_repo.GetEntityByID(Convert.ToInt32(listbox_entities.SelectedValue.ToString()));
            b.Price = price;
            b.Title = title;
            b.Author = author;


            book_repo.Update(b);
            BookVM t = (BookVM)this.DataContext;
            ObservableCollection<Book> k = t.Books;
            foreach (Book b2 in k)
            {
                if (b.ID == b2.ID)
                {
                    b2.Author = b.Author;
                    b2.Title = b.Title;
                    b2.Price = b.Price;
                }
            }
            t.RaiseChange("Students");
        }

        public void InsertInDB()
        {
            title = txt_title.Text;
            price = Convert.ToDecimal(txt_price.Text);
            author = txt_author.Text;

            Book b = new Book();
            b.Price = price;
            b.Title = title;
            b.Author = author;

            book_repo.Insert(b);
            BookVM t = (BookVM)this.DataContext;
            t.Refresh();
        }
    }
}
