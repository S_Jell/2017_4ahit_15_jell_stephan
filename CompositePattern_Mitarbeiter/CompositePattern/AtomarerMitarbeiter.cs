﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompositePattern
{
    public class AtomarerMitarbeiter : Mitarbeiter
    {
        public override void print(string abstand)
        {
            Console.WriteLine(abstand + getName() + ". Tel: " + getTelefonNr());
        }

        public AtomarerMitarbeiter(string name, int telefonNr) : base(name, telefonNr) { }

        public override int getMitarbeiterAnzahl()
        {
            return 1;
        }
    }
}
