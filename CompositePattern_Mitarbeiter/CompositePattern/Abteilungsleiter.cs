﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompositePattern
{
    public class Abteilungsleiter : Mitarbeiter
    {
        public override void print(string abstand)
        {
            Console.WriteLine(abstand + "Abteilungsleiter " + getName() + " (" + getAbteilung() + "). Tel: " + getTelefonNr());
            foreach(Mitarbeiter ma in mitarbeiter)
            {
                ma.print(abstand + "      ");
            }
        }

        private List<Mitarbeiter> mitarbeiter = new List<Mitarbeiter>();
        private string abteilung;

        public string getAbteilung()
        {
            return abteilung;
        }

        public void setAbteilung(string abteilung)
        {
            this.abteilung = abteilung;
        }

        public Abteilungsleiter(string name, string abteilung, int telefonNr) : base(name,telefonNr)
        {
            this.abteilung = abteilung;
        }

        public override int getMitarbeiterAnzahl()
        {
            int summe = 1; //1 für sich selbst
            foreach (Mitarbeiter ma in mitarbeiter)
            {
                summe += ma.getMitarbeiterAnzahl();
            }
            return summe;
        }

        public void add(Mitarbeiter ma)
        {
            mitarbeiter.Add(ma);
        }

        public void remove(Mitarbeiter ma)
        {
            mitarbeiter.Remove(ma);
        }

        public Mitarbeiter getMitarbeiter(int index)
        {
            foreach (Mitarbeiter m in mitarbeiter)
            {
                if (mitarbeiter.IndexOf(m) == index)
                    return m;
            }
            return null;
        }
    }
}
