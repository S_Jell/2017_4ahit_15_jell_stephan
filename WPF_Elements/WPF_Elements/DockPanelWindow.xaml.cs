﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WPF_Elements
{
    /// <summary>
    /// Interaktionslogik für DockPanelWindow.xaml
    /// </summary>
    public partial class DockPanelWindow : Window
    {
        public DockPanelWindow()
        {
            InitializeComponent();
        }

        private void btn_red_Click(object sender, RoutedEventArgs e)
        {
            change_color(Brushes.Red);
        }

        private void btn_yellow_Click(object sender, RoutedEventArgs e)
        {
            change_color(Brushes.Yellow);
        }

        private void btn_green_Click(object sender, RoutedEventArgs e)
        {
            change_color(Brushes.Green);
        }

        private void btn_blue_Click(object sender, RoutedEventArgs e)
        {
            change_color(Brushes.Blue);
        }

        private void change_color(Brush c)
        {
            btn_blue.Background = c;
            btn_green.Background = c;
            btn_red.Background = c;
            btn_yellow.Background = c;
        }
    }
}
