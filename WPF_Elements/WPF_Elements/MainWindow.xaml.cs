﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPF_Elements
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btn_grid_Click(object sender, RoutedEventArgs e)
        {
            WindowGrid wingrid = new WindowGrid();
            wingrid.Show();
            this.Close();
        }
       

        private void btn_dockpanel_click(object sender, RoutedEventArgs e)
        {
            DockPanelWindow dp = new DockPanelWindow();
            this.Close();
            dp.Show();
        }

        private void btn_canvas_Click(object sender, RoutedEventArgs e)
        {
            CanvasWindow cw = new CanvasWindow();
            this.Close();
            cw.Show();
            
        }

        private void btn_wrappanel_Click(object sender, RoutedEventArgs e)
        {
            WrapPanelWindow wp = new WrapPanelWindow();
            this.Close();
            wp.Show();
        }
    }
}
