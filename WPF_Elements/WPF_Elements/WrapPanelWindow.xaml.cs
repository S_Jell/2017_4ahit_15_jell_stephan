﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


namespace WPF_Elements
{
    /// <summary>
    /// Interaktionslogik für WrapPanelWindow.xaml
    /// </summary>
    public partial class WrapPanelWindow : Window
    {
        public WrapPanelWindow()
        {
            InitializeComponent();
        }

        private void submit_Click(object sender, RoutedEventArgs e)
        {
            lb_01.Content = new TextRange(rtb_01.Document.ContentStart, rtb_01.Document.ContentEnd).Text;
        }


        private void tb_01_Checked(object sender, RoutedEventArgs e)
        {
            var da = new DoubleAnimation(360, 0, new Duration(TimeSpan.FromSeconds(1)));
            var rt = new RotateTransform();
            lb_01.RenderTransform = rt;
            lb_01.RenderTransformOrigin = new Point(0.5, 0.5);
            da.RepeatBehavior = RepeatBehavior.Forever;
            rt.BeginAnimation(RotateTransform.AngleProperty, da);
        }

        private void tb_01_Unchecked(object sender, RoutedEventArgs e)
        {

        }
    }
}
