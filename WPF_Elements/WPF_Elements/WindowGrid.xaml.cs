﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WPF_Elements
{
    /// <summary>
    /// Interaktionslogik für WindowGrid.xaml
    /// </summary>
    public partial class WindowGrid : Window
    {
        public WindowGrid()
        {
            InitializeComponent();
        }

        private void click_register(object sender, RoutedEventArgs e)
        {
            
            string gender = check_gender();
            MessageBox.Show("Successfully registered user: " + txt_first_name.Text + " " + txt_last_name.Text + " (" + gender + ")");
        }
        private string check_gender()
        {
            if (Convert.ToBoolean(rb_female.IsChecked))
                return "female";
            else
                return "male";
        }
    }
}
