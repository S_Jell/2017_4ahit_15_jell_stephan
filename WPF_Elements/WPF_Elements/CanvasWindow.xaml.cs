﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WPF_Elements
{
    /// <summary>
    /// Interaktionslogik für CanvasWindow.xaml
    /// </summary>
    public partial class CanvasWindow : Window
    {
        public CanvasWindow()
        {
            InitializeComponent();
        }

        private void ck_rectangle_Checked(object sender, RoutedEventArgs e)
        {
            ListViewItem tmp = new ListViewItem();
            tmp.Content = "Rectangle";
            lv_01.Items.Add(tmp);
            ck_rectangle.IsChecked = false;
        }

        private void ck_ellipse_Checked(object sender, RoutedEventArgs e)
        {
            ListViewItem tmp = new ListViewItem();
            tmp.Content = "Ellipse";
            lv_01.Items.Add(tmp);
            ck_ellipse.IsChecked = false;
        }

        private void ck_triangle_Checked(object sender, RoutedEventArgs e)
        {
            ListViewItem tmp = new ListViewItem();
            tmp.Content = "Triangle";
            lv_01.Items.Add(tmp);
            ck_triangle.IsChecked = false;
        }

        private void submit_Click(object sender, RoutedEventArgs e)
        {
            string shape = check_shape();
            string color = check_color();

            if (!(shape == null && color == null))
            {
                Label lb_tmp = new Label();
                lb_tmp.Content = shape + " " + color;
                lb_01.Items.Add(lb_tmp);
            }
        }
        private string check_shape()
        {
            if (ck_ellipse02.IsChecked == true)
                return "ellipse";
            if (ck_rectangle02.IsChecked == true)
                return "rectangle";
            if ((ck_ellipse02.IsChecked == true) && (ck_rectangle02.IsChecked == true))
                return null;
            return null;
        }
        private string check_color()
        {
            if (ck_red.IsChecked == true)
                return "red";
            if (ck_blue.IsChecked == true)
                return "blue";
            if ((ck_blue.IsChecked == true) && (ck_red.IsChecked == true))
                return null;
            return null;

        }
    }
}
