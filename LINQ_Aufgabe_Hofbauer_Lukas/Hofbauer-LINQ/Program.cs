﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hofbauer_LINQ
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numbers = { 23, 54, 4, 12, 45, 21, 3, 12, 54, 358, 54, 454, 211, 481, 45, 14, 8 };

            var query1 = from num in numbers where num > 10 && num < 400 select num;

            foreach(var item in query1)
            {
                Console.Write($"{item} ");
            }

            Console.WriteLine("\n");

            var query2 = (from num in numbers where (num % 3 == 0) select num).Distinct().OrderBy(x => x);

            foreach (var item in query2)
            {
                Console.Write($"{item} ");
            }

            Console.WriteLine("\n");


            int[] inNumbers1 = { 28, 54, 4, 32, 23, 21, 3, 12, 54, 358, 90, 454, 43, 481, 45, 14, 65 };
            int[] inNumbers2 = { 23, 54, 7, 12, 45, 21, 3, 43, 522, 32, 88, 78, 211, 89, 34, 14, 82 };
            int[] inNumbers3 = { 70, 54, 4, 56, 45, 42, 3, 76, 54, 38, 54, 4, 211, 41, 12, 35, 2 };

            var query3 = (from num in inNumbers1.Union(inNumbers2).Union(inNumbers3) where num < 200 select num).Distinct().Count();

            Console.WriteLine($"{query3} \n");

            List<Person> people = new List<Person>();

            people.Add(new Person { ID = 1, FirstName = "Gregor", LastName = "Beutlin", Country = "Oesterreich" });
            people.Add(new Person { ID = 2, FirstName = "Heinz", LastName = "Gross", Country = "Deutschland" });
            people.Add(new Person { ID = 3, FirstName = "Jörg", LastName = "Zaun", Country = "Oesterreich" });
            people.Add(new Person { ID = 4, FirstName = "Michael", LastName = "Schratti", Country = "Deutschland" });
            people.Add(new Person { ID = 5, FirstName = "Stefan", LastName = "Hauser", Country = "Schweiz" });
            people.Add(new Person { ID = 6, FirstName = "Christoph", LastName = "Falz", Country = "Oesterreich" });
            people.Add(new Person { ID = 7, FirstName = "Julius", LastName = "Pulbus", Country = "England" });
            people.Add(new Person { ID = 8, FirstName = "Heinz", LastName = "Kron", Country = "Deutschland" });
            people.Add(new Person { ID = 9, FirstName = "Ernst", LastName = "Peter", Country = "Oesterreich" });
            people.Add(new Person { ID = 10, FirstName = "Peter", LastName = "Griffin", Country = "England" });
            people.Add(new Person { ID = 11, FirstName = "Frank", LastName = "Jaker", Country = "England" });


            var query4 = (from pers in people select pers).GroupBy(x => x.Country).OrderBy(x => x.Key);

            foreach(var key in query4)
            {
                Console.WriteLine(key.Key);
                foreach(var value in key.Select(p=>p).OrderBy(p=>p.LastName))
                {
                    Console.WriteLine($"{value.ID} , {value.FirstName} , {value.LastName}");
                }
                Console.WriteLine("\n");
            }


        }
    }
}
