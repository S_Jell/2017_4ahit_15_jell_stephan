﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;

namespace AsyncAwaitFileAccess
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public string Path = "TEST.txt";
        public MainWindow()
        {
            InitializeComponent();
        }

        private Int32 CountChars(string FilePath)
        {
            Int32 count = new int();
            try
            {
                using (StreamReader streamReader = new StreamReader(FilePath))
                {
                    string content = streamReader.ReadToEnd();
                    count = content.Length;
                }
            }
            catch (FileNotFoundException)
            {
                MessageBox.Show("FILE NOT FOUND!");
            }
            Thread.Sleep(10000); //10 sec LAAAAAGGGGG
            return count;

        }

        private async void btn_processing_Click(object sender, RoutedEventArgs e)
        {
            Task<int> task = new Task<int>(() => CountChars(Path));
            task.Start();
            lbl_processing.Content = "Processing!";
            int count = await task;
            lbl_processing.Content = $"The file contains {count} chars";
        }

        private void btn_pres_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("I'm just a helper!");
        }
    }
}    
