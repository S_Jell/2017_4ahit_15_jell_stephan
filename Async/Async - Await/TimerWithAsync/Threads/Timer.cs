﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Threads
{
    class Timer
    {
        private CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
        private CancellationToken cancellationToken;
        public Action Action { get; set; }
        public int Interval { get; set; }
       
        public Timer(Action action, int interval)
        {
            Action = action;
            Interval = interval;
        }

        public void Start()
        {
            cancellationToken = cancellationTokenSource.Token;
            Task.Run(async () =>
            {
                while (true)
                {
                    Action();
                    await Task.Delay(Interval);
                    if (cancellationToken.IsCancellationRequested)
                    {
                        cancellationToken.ThrowIfCancellationRequested();
                    }
                }
            }, cancellationToken)
            .ContinueWith(o =>
            {
                Console.WriteLine("Exception was raised in the timers action: " + o.Exception.Message);
                throw new TimerActionException(o.Exception.Message + " was raised");
            }, TaskContinuationOptions.OnlyOnFaulted);  
        }

        public void Stop()
        {
            cancellationTokenSource.Cancel();
        }
    }
}
