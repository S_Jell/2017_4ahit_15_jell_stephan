﻿using System;
using System.Threading.Tasks;

namespace Threads
{
    class Program
    {
        static void Main(string[] args)
        {
            int numOfCalls = 0;
            Timer t = new Timer(() =>
            {
                Console.WriteLine("Test");
                numOfCalls++;
                if (numOfCalls == 3)
                {
                    throw new InvalidOperationException();
                }
            }, 2000);
            t.Start();
            System.Threading.Thread.Sleep(10000);
            t.Stop();
            Console.ReadLine();
        }
    }
}
