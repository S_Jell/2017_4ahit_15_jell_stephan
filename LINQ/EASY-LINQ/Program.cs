﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EASY_LINQ
{
    class Program
    {
        static void Main(string[] args)
        {
            Student[] studentArray = {
                    new Student() { StudentID = 1, StudentName = "John", age = 18 } ,
                    new Student() { StudentID = 2, StudentName = "Steve",  age = 21 } ,
                    new Student() { StudentID = 3, StudentName = "Bill",  age = 25 } ,
                    new Student() { StudentID = 4, StudentName = "Ram" , age = 20 } ,
                    new Student() { StudentID = 5, StudentName = "Ron" , age = 31 } ,
                    new Student() { StudentID = 6, StudentName = "Chris",  age = 17 } ,
                    new Student() { StudentID = 7, StudentName = "Rob",age = 19  } ,
                };

            List<Student> students = studentArray.Where(s => s.age > 12 && s.age < 20).ToList();

            Student bill = studentArray.Where(s => s.StudentName == "Bill").FirstOrDefault();

            Student student5 = studentArray.Where(s => s.StudentID == 5).FirstOrDefault();

            foreach (var stu in students)
            {
                Console.WriteLine(stu.StudentName);
            }
            Console.WriteLine(bill.StudentName);

            Console.WriteLine(student5.StudentName);

            Console.WriteLine("\n\n");
            IList<Student> studentList = new List<Student> () {
                new Student() { StudentID = 1, StudentName = "John", age = 13 } ,
                new Student() { StudentID = 2, StudentName = "Moin", age = 21 } ,
                new Student() { StudentID = 3, StudentName = "Bill", age = 18 } ,
                new Student() { StudentID = 4, StudentName = "Ram", age = 20 } ,
                new Student() { StudentID = 5, StudentName = "Ron", age = 15 }
            };

            // LINQ Query Syntax to find out teenager students
            var teenAgerStudent = from s in studentList
                                  where s.age > 12 && s.age < 20
                                  select s;

            foreach (var stu in teenAgerStudent)
            {
                Console.WriteLine(stu.StudentName);
            }
            Console.ReadKey();
        }
    }
}
