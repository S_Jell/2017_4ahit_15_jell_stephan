﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnonymousType
{
    class Program
    {
        static void Main(string[] args)
        {
            var nestedAnonymousType = new
            {
                firstProperty = "First",
                secondProperty = 2,
                thirdProperty = true,
                anotherAnonymousType = new { nestedProperty = "Nested" }
            };

            var myAnonymousType = new
            {
                firstProperty = "First Property",
                secondProperty = 2,
                thirdProperty = true
            };

            DoSomethig(myAnonymousType);
        }


        static void DoSomethig(dynamic param)
        {
            Console.WriteLine($"{param.firstProperty} , {param.secondProperty} , {param.thirdProperty}");
        }

    }
}
