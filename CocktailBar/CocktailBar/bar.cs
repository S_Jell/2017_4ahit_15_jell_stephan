﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace CocktailBar
{
    class bar
    {
        private static SemaphoreSlim barenter = new SemaphoreSlim(0);
        private static Semaphore mixer = new Semaphore(0,3);
        public void Test(int i)
        {
            
            for (int j = 0; j < i; j++)
            {
                Thread t1 = new Thread(Serve);
                t1.Name = "Kellner " + j;
                t1.Start();
            }
            
            Console.WriteLine("\nBesprechung");
            Thread.Sleep(2000);
            barenter.Release(10);
            mixer.Release(3);
        }
        private void Serve()
        {
            
                barenter.Wait();
                Console.WriteLine("{0} betritt die Bar", Thread.CurrentThread.Name);
                
                MixCocktail();
                Console.WriteLine("{0} verlässt die Bar", Thread.CurrentThread.Name);
                barenter.Release();
            
        }
        private void MixCocktail()
        {
            mixer.WaitOne();
            Console.WriteLine("{0} nutzt den Mixer", Thread.CurrentThread.Name);
            Thread.Sleep(2000);
            Console.WriteLine("{0} gibt den Mixer frei", Thread.CurrentThread.Name);
            mixer.Release();
        }
        
    }
}
