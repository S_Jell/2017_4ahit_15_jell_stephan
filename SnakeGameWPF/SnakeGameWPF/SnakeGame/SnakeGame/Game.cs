﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Windows.Controls;
using System.Windows.Input;

namespace SnakeGame
{
    class Game
    {
        public GameData gamedata;

        Canvas canvas;


        public List<Snake> snakebody;
        public double x = 100; //position of snake
        public double y = 100;

        public Food food;

        public Random rd = new Random();

        public Game(Canvas c)
        {
            gamedata = new GameData();
            canvas = c;
            snakebody = new List<Snake>();
            snakebody.Add(new Snake(x, y));
            food = new Food(rd.Next(0, 45) * 10, rd.Next(0, 34) * 10);
        }

        public void addFood()
        {
            food.setfoodposition();
            canvas.Children.Insert(0, food.ell);
        }

        public void addSnake()
        {
            foreach (Snake snake in snakebody)
            {
                snake.setsnakeposition();
                canvas.Children.Add(snake.rec);
            }
        }

        public void setDirection(KeyEventArgs e)
        {
            if (e.Key == Key.Up && gamedata.direction != gamedata.down)
                gamedata.direction = gamedata.up;
            if (e.Key == Key.Down && gamedata.direction != gamedata.up)
                gamedata.direction = gamedata.down;
            if (e.Key == Key.Left && gamedata.direction != gamedata.right)
                gamedata.direction = gamedata.left;
            if (e.Key == Key.Right && gamedata.direction != gamedata.left)
                gamedata.direction = gamedata.right;
        }

        public void copySnakePlace() /////////////////////////////////////////////////
        {
            if (gamedata.direction != 0)
            {
                for (int i = snakebody.Count - 1; i > 0; i--)
                {
                    snakebody[i] = snakebody[i - 1];
                }
            }
        }

        public void moveSnake()
        {
            if (gamedata.direction == gamedata.up)
                y -= 10;
            if (gamedata.direction == gamedata.down)
                y += 10;
            if (gamedata.direction == gamedata.left)
                x -= 10;
            if (gamedata.direction == gamedata.right)
                x += 10;
        }

        public void eat(Label scorelabel)
        {
            if (snakebody[0].x == food.x && snakebody[0].y == food.y)
            {
                snakebody.Add(new Snake(food.x, food.y));
                food = new Food(rd.Next(0, 45) * 10, rd.Next(0, 34) * 10);
                canvas.Children.RemoveAt(0);
                addFood();
                gamedata.Score = gamedata.Score +1;
                //scorelabel.Content = "Score: " + score.ToString();
            }
        }

        public void end(MainWindow w)
        {
            if (snakebody[0].x > 490 || snakebody[0].y > 360 || snakebody[0].x < 0 || snakebody[0].y < 0) 
            {
                w.Close();
            }

            for (int i = 1; i < snakebody.Count; i++)
            {
                if (snakebody[0].x == snakebody[i].x && snakebody[0].y == snakebody[i].y)
                    w.Close();
            }
        }

        public void removePreviousSnakeElement()
        {
            for (int i = 0; i < canvas.Children.Count; i++)
            {
                if (canvas.Children[i] is Rectangle)
                    gamedata.count++;
            }
            canvas.Children.RemoveRange(1, gamedata.count);
            gamedata.count = 0;
            addSnake();
        }

        
    }
}
