﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace SnakeGame
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Game game;
        DispatcherTimer time;

        public MainWindow()
        {
            
            InitializeComponent();

            time = new DispatcherTimer();
            game = new Game(mycanvas);

            DataContext = game.gamedata;

            time.Interval = new TimeSpan(0, 0, 0, 0, 100); //change of speed 
            time.Tick += time_Tick;
        }

        void time_Tick(object sender, EventArgs e)
        {

            game.copySnakePlace();
            game.moveSnake();
            game.eat(scorelabel);
            game.snakebody[0] = new Snake(game.x, game.y);
            game.end(this);
            game.removePreviousSnakeElement();
        }


        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            game.setDirection(e);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            game.addSnake();
            game.addFood();
            time.Start();
        }
    }
}
